from Bio import SeqIO
from Bio.PDB import Polypeptide
from transformers import BertModel, BertTokenizer, pipeline
from pepnn_seq.models import FullModel
from pepnn_seq.models import score
import pandas as pd
import numpy as np
import torch
import argparse
import os


def to_var(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return x


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

   
    parser.add_argument("-prot", dest="input_protein_file", required=False, type=str,
                        help="Fasta file with protein sequence")
    
    parser.add_argument("-pep", dest="input_peptide_file", required=False, type=str, default=None,
                        help="Fasta file with peptide sequence")

    
   
    parser.add_argument("-o", dest="output_directory", required=False, type=str, default=None,
                        help="Output directory")
   
    
    parser.add_argument("-p", dest="params", required=False, type=str, default="../params/params.pth",
                        help="Model parameters")
   
    args = parser.parse_args()
 
    if args.output_directory == None:
        output_directory = os.path.split(args.input_protein_file)[-1].split(".")[0] + "_seq"
    else:
        output_directory = args.output_directory
    
    if not os.path.exists(output_directory):
        os.mkdir(output_directory)
    
 
    records = SeqIO.parse(args.input_protein_file, format="fasta")
    
    prot_sequence = ' '.join(list(records)[0].seq)
        
   
    protbert_dir = os.path.join(os.path.dirname(__file__), '../models/ProtBert-BFD/')
    
    vocabFilePath = os.path.join(protbert_dir, 'vocab.txt')
    tokenizer = BertTokenizer(vocabFilePath, do_lower_case=False )
    seq_embedding = BertModel.from_pretrained(protbert_dir)
    
    if  torch.cuda.is_available():
        seq_embedding = pipeline('feature-extraction', model=seq_embedding, tokenizer=tokenizer, device=0)
    else:
        seq_embedding = pipeline('feature-extraction', model=seq_embedding, tokenizer=tokenizer, device=-1)
    
    embedding = seq_embedding(prot_sequence)

    embedding = np.array(embedding)
    
    seq_len = len(prot_sequence.replace(" ", ""))
    start_Idx = 1
    end_Idx = seq_len+1
    seq_emd = embedding[0][start_Idx:end_Idx]


    prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0))
    
    
    
    if args.input_peptide_file != None:
    
        records = SeqIO.parse(args.input_peptide_file, format="fasta")
    
        pep_sequence = str(list(records)[0].seq).replace("X", "")
        
        pep_sequence = [Polypeptide.d1_to_index[i] for i in pep_sequence]
        
    else:
        
        pep_sequence = [5 for i in range(10)]
        
    pep_seq = to_var(torch.LongTensor(pep_sequence).unsqueeze(0))
    
   
    model = FullModel(6, 64, 6, 
                      
                      64, 128, 64, dropout=0.2)
    if torch.cuda.is_available():
        model.load_state_dict(torch.load(os.path.join(os.path.dirname(__file__), args.params)))
    else:
        model.load_state_dict(torch.load(os.path.join(os.path.dirname(__file__), args.params),
                                         map_location='cpu'))

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        model.cuda()
    
    
    model.eval()
    
    if torch.cuda.is_available():
        outputs = model(pep_seq, prot_seq).cpu().detach().numpy()
    else:
        outputs = model(pep_seq, prot_seq).detach().numpy()
    
    # compute score for the domain and output file
    
    score_prm = score(outputs)
    
    with open(output_directory + "/prm_score.txt", 'w') as output_file:
        
        output_file.writelines("The input protein's score is {0:.2f}".format(score_prm))
 
    
    # output prediction as csv
    
    outputs = np.exp(outputs[0])
    
    amino_acids = []
    
    probabilities = []
    
    position = []
    for index, aa in enumerate(prot_sequence.split(" ")):
        probabilities.append(outputs[index, 1])
        amino_acids.append(aa)
        position.append(index+1)
        

    output = pd.DataFrame()
    
    output["Position"] = position
    output["Amino acid"] =  amino_acids
    output["Probabilities"] = probabilities
    
    
    
    output.to_csv(output_directory + "/binding_site_prediction.csv", index=False)
    