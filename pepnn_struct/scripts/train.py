# -*- coding: utf-8 -*-
"""
Created on Sat Jul 31 19:40:31 2021

@author: Osama
"""

from pepnn_struct.models import FullModel
from pepnn_struct.models import PeptideComplexes, PepBindComplexes, InterpepComplexes, BitenetComplexes
from transformers import BertModel, BertTokenizer, pipeline
from time import time
import numpy as np
import torch
import argparse
import torch.nn.functional as F
import datetime
import os

def to_var(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return x


def train_pepnn(model, epochs, output_file):
    binding_weights = to_var(torch.FloatTensor(np.load("../params/binding_weights.npy")))
    
   
    
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0001)
    
    

    
    
    g = torch.Generator()
    g.manual_seed(0)
    
    loader = torch.utils.data.DataLoader(PeptideComplexes(mode="train"),
                                         batch_size=1, num_workers=1,
                                         shuffle=True)
    
                                                                                                                                                                                                      
    validation_loader = torch.utils.data.DataLoader(PeptideComplexes(mode="val"),
                                                    batch_size=1, num_workers=1,
                                                    shuffle=True)
        
    start_time = time()
    
    log_step = 100
    
    
    validation_losses = []
    
    iters = len(loader)
    
    iters_validation = len(validation_loader)

    protbert_dir = os.path.join(os.path.dirname(__file__), '../../pepnn_seq/models/ProtBert-BFD/')
    
    vocabFilePath = os.path.join(protbert_dir, 'vocab.txt')
    tokenizer = BertTokenizer(vocabFilePath, do_lower_case=False )
    seq_embedding = BertModel.from_pretrained(protbert_dir)
    seq_embedding = pipeline('feature-extraction', model=seq_embedding, tokenizer=tokenizer, device=0)
    
    for e in range(epochs):
        
        model.train()
        run_loss = 0
        accuracy = 0
        
        for value, (pep_sequence, edges, nodes, neighbor_indices, target, weight, prot_seq) in enumerate(loader):

            embedding = seq_embedding(prot_seq[0])

            embedding = np.array(embedding)
            
            seq_len = len(prot_seq[0].replace(" ", ""))
            start_Idx = 1
            end_Idx = seq_len+1
            seq_emd = embedding[0][start_Idx:end_Idx]
    

            prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0)) 
            
            weight = to_var(weight)
            edges = to_var(edges)
            
            nodes = to_var(nodes)
            neighbor_indices = to_var(neighbor_indices)
            pep_sequence = to_var(pep_sequence)
            target = to_var(target)


            optimizer.zero_grad()
            
            outputs_nodes = model(pep_sequence, nodes, edges, neighbor_indices, prot_seq)
            
            
      
            loss = F.nll_loss(outputs_nodes.transpose(-1,-2), target,
                              ignore_index=-100, weight=binding_weights)

            
        
            loss = weight*loss

            run_loss += loss.item()
            
            loss.backward()
            
            optimizer.step()
            
            predict = torch.argmax(outputs_nodes, dim=-1)
          
            
            predict = predict.cpu().detach().numpy()
            actual = target.cpu().detach().numpy()
            
            
            correct = np.sum(predict == actual)
            total = np.sum(len(actual[0]))
            
            
            accuracy +=  correct/total
            
            
                      
            


                
            if (value + 1) % log_step == 0 or value == iters - 1:

                elapsed = time() - start_time
                elapsed = str(datetime.timedelta(seconds=elapsed))
                if value == iters -1: 
                    div = (iters) % log_step      
                else:
                    div = log_step 
                log = "Elapsed [{}], Epoch [{}/{}], Iter [{}/{}]".format(
                    elapsed, e+1, epochs, value + 1, iters)
                log += ", {}: {:.5f}".format('CE', run_loss / div)
                log += ", {}: {:.5f}".format('Accuracy', accuracy / div)
                print(log)
                
                
                run_loss = 0
                accuracy = 0
                
            if (value+1) % 100 == 0:
                model.eval()
                run_loss = 0
                accuracy = 0
                with torch.no_grad():
                    for value_val, (pep_sequence, edges, nodes, neighbor_indices, target, weight, prot_seq) in enumerate(validation_loader):
                        embedding = seq_embedding(prot_seq[0])

                        embedding = np.array(embedding)
            
                        seq_len = len(prot_seq[0].replace(" ", ""))
                        start_Idx = 1
                        end_Idx = seq_len+1
                        seq_emd = embedding[0][start_Idx:end_Idx]
    

                        prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0)) 
                        
                        
                        weight = to_var(weight)
                        edges = to_var(edges)
                        nodes = to_var(nodes)
                        neighbor_indices = to_var(neighbor_indices)
                        pep_sequence = to_var(pep_sequence)

                        target = to_var(target)
                        
                        outputs_nodes = model(pep_sequence, nodes, edges, neighbor_indices, prot_seq)
                       
                       
                        
                                            
                        loss = F.nll_loss(outputs_nodes.transpose(-1,-2), target,
                              ignore_index=-100, weight=binding_weights)


                        loss = loss*weight
                        run_loss += loss.item()
                        
                    
                        predict = torch.argmax(outputs_nodes, dim=-1)
                      
                        
                        predict = predict.cpu().detach().numpy()
                        actual = target.cpu().detach().numpy()
                        
                        
                        
                        correct = np.sum(predict == actual)
                        total = np.sum(len(actual[0]))
                        
                    
            
                        accuracy += correct/total                        
                    
                        del loss
                
                validation_losses.append(run_loss / iters_validation)
               
                                
                log = "This is validation, Epoch [{}/{}]".format(
                        e + 1, epochs)
        
                log += ", {}: {:.5f}".format('CE', validation_losses[-1])
                
                log += ", {}: {:.5f}".format('Accuracy', accuracy / (iters_validation))
                
                
                if validation_losses[-1]  == min(validation_losses):
                        print("Saving model with new minimum validation loss")
                        torch.save(model.state_dict(),output_file)  
        
                        
                        print("Saved model successfully!")
                
                print(log)

                
                run_loss = 0
                accuracy = 0
                
                model.train()
                
def train_bitenet(model, epochs, output_file):
    
    binding_weights = to_var(torch.FloatTensor(np.load("../params/binding_weights_bitenet.npy")))
    
    protbert_dir = os.path.join(os.path.dirname(__file__), '../../pepnn_seq/models/ProtBert-BFD/')
    
    vocabFilePath = os.path.join(protbert_dir, 'vocab.txt')
    tokenizer = BertTokenizer(vocabFilePath, do_lower_case=False )
    seq_embedding = BertModel.from_pretrained(protbert_dir)
    seq_embedding = pipeline('feature-extraction', model=seq_embedding, tokenizer=tokenizer, device=0)
    
    
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0001)
    
    
    g = torch.Generator()
    g.manual_seed(0)
    
    loader = torch.utils.data.DataLoader(BitenetComplexes(),
                                         batch_size=1, num_workers=1,
                                         shuffle=True)


        
    start_time = time()
    
 
    

    
    iters = len(loader)
    
    
    for e in range(epochs):
        
        model.train()
        run_loss = 0
        accuracy = 0
        for value, (pep_sequence, edges, nodes, neighbor_indices, target, prot_seq) in enumerate(loader):

            embedding = seq_embedding(prot_seq[0])

            embedding = np.array(embedding)
            
            seq_len = len(prot_seq[0].replace(" ", ""))
            start_Idx = 1
            end_Idx = seq_len+1
            seq_emd = embedding[0][start_Idx:end_Idx]

    

            prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0)) 
            
            edges = to_var(edges)
            
            nodes = to_var(nodes)
            neighbor_indices = to_var(neighbor_indices)
            pep_sequence = to_var(pep_sequence)
            target = to_var(target)


            optimizer.zero_grad()
            
            outputs_nodes = model(pep_sequence, nodes, edges, neighbor_indices, prot_seq)
            
            
      
            loss = F.nll_loss(outputs_nodes.transpose(-1,-2), target,
                              ignore_index=-100, weight=binding_weights)

            
        
            

            run_loss += loss.item()
            
            loss.backward()
            
            optimizer.step()
            
            predict = torch.argmax(outputs_nodes, dim=-1)
          
            
            predict = predict.cpu().detach().numpy()
            actual = target.cpu().detach().numpy()
            
            
            correct = np.sum(predict == actual)
            total = np.sum(len(actual[0]))
            
            
            accuracy +=  correct/total
            
            
                      
            


                
            
        elapsed = time() - start_time
        log = "Elapsed [{}], Epoch [{}/{}], Iter [{}/{}]".format(
            elapsed, e+1, epochs, value + 1, iters)
        log += ", {}: {:.5f}".format('CE', run_loss / iters)
        log += ", {}: {:.5f}".format('Accuracy', accuracy / iters)
        print(log)
    
        
        run_loss = 0
        accuracy = 0
               
                
            
    print("Saving model!")
    torch.save(model.state_dict(),output_file)  

    
    print("Saved model successfully!")
    
def train(model, epochs, output_file, dataset):
    
    
    
    

    if dataset == "pepbind":
     
        
        loader = torch.utils.data.DataLoader(PepBindComplexes(mode="train"),
                                             batch_size=1, num_workers=1,
                                             shuffle=True)
    
        validation_loader = torch.utils.data.DataLoader(PepBindComplexes(mode="val"),
                                                        batch_size=1, num_workers=1,
                                                        shuffle=True)
        
        
        binding_weights = to_var(torch.FloatTensor(np.load("../params/binding_weights_pepbind.npy")))
    elif dataset == "interpep":
        
        loader = torch.utils.data.DataLoader(InterpepComplexes(mode="train"),
                                             batch_size=1, num_workers=1,
                                             shuffle=True)
    
        validation_loader = torch.utils.data.DataLoader(InterpepComplexes(mode="val"),
                                                        batch_size=1, num_workers=1,
                                                        shuffle=True)
        
        
        binding_weights = to_var(torch.FloatTensor(np.load("../params/binding_weights_interpep.npy")))
        
    start_time = time()
    
    
    
    
    validation_losses = []
    
    iters = len(loader)
    
    iters_validation = len(validation_loader)
    
    
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0001)
    
    
    protbert_dir = os.path.join(os.path.dirname(__file__), '../../pepnn_seq/models/ProtBert-BFD/')
    
    vocabFilePath = os.path.join(protbert_dir, 'vocab.txt')
    tokenizer = BertTokenizer(vocabFilePath, do_lower_case=False )
    seq_embedding = BertModel.from_pretrained(protbert_dir)
    seq_embedding = pipeline('feature-extraction', model=seq_embedding, tokenizer=tokenizer, device=0)
    
    
    for e in range(0, epochs):
        
        model.train()
        run_loss = 0
        accuracy = 0
        for value, (pep_sequence, edges, nodes, neighbor_indices, target, prot_seq) in enumerate(loader):

            embedding = seq_embedding(prot_seq)
        
            embedding = np.array(embedding)
            
            seq_len = len(prot_seq.replace(" ", ""))
            start_Idx = 1
            end_Idx = seq_len+1
            seq_emd = embedding[0][start_Idx:end_Idx]
        
        
            prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0))
            
            edges = to_var(edges)
            
            nodes = to_var(nodes)
            neighbor_indices = to_var(neighbor_indices)
            pep_sequence = to_var(pep_sequence)
            target = to_var(target)


            optimizer.zero_grad()
            
            outputs_nodes = model(pep_sequence, nodes, edges, neighbor_indices, prot_seq)
            
            
      
            loss = F.nll_loss(outputs_nodes.transpose(-1,-2), target,
                              ignore_index=-100, weight=binding_weights)

            
        
            
            run_loss += loss.item()
            
            loss.backward()
            
            optimizer.step()
            
            predict = torch.argmax(outputs_nodes, dim=-1)
          
            
            predict = predict.cpu().detach().numpy()
            actual = target.cpu().detach().numpy()
            
            
            correct = np.sum(predict == actual)
            total = len(actual[0])
            
            
            accuracy +=  correct/total
            
            
                      
            
           
                
            if value == iters - 1:

                elapsed = time() - start_time
                elapsed = str(datetime.timedelta(seconds=elapsed))
                div =  iters
                log = "Elapsed [{}], Epoch [{}/{}], Iter [{}/{}]".format(
                    elapsed, e+1, epochs, value + 1, iters)
                log += ", {}: {:.5f}".format('CE', run_loss / div)
                log += ", {}: {:.5f}".format('Accuracy', accuracy / div)
                print(log)
                
                
                run_loss = 0
                accuracy = 0
               
                
            if value == iters - 1:
                model.eval()
                run_loss = 0
                accuracy = 0
                with torch.no_grad():
                    for value_val, (pep_sequence, edges, nodes, neighbor_indices, target, prot_seq) in enumerate(validation_loader):
                        embedding = seq_embedding(prot_seq)
                    
                        embedding = np.array(embedding)
                        
                        seq_len = len(prot_seq.replace(" ", ""))
                        start_Idx = 1
                        end_Idx = seq_len+1
                        seq_emd = embedding[0][start_Idx:end_Idx]
                    
                    
                        prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0))
                        
                        edges = to_var(edges)
                        nodes = to_var(nodes)
                        neighbor_indices = to_var(neighbor_indices)
                        pep_sequence = to_var(pep_sequence)

                        target = to_var(target)
                        
                        outputs_nodes = model(pep_sequence, nodes, edges, neighbor_indices, prot_seq)
                       
                       
                        
                        
                    
                        loss = F.nll_loss(outputs_nodes.transpose(-1,-2), target,
                              ignore_index=-100, weight=binding_weights)


                        run_loss += loss.item()
                        
                    
                        predict = torch.argmax(outputs_nodes, dim=-1)
                      
                        
                        predict = predict.cpu().detach().numpy()
                        actual = target.cpu().detach().numpy()
                        
                    
                        
                        correct = np.sum(predict == actual)
                        total = len(actual[0])
                        
                    
            
                        accuracy += correct/total                        
                    
                        del loss
                
                validation_losses.append(run_loss / iters_validation)
               
                                
                log = "This is validation, Epoch [{}/{}]".format(
                        e + 1, epochs)
        
                log += ", {}: {:.5f}".format('CE', validation_losses[-1])
                
                log += ", {}: {:.5f}".format('Accuracy', accuracy / (iters_validation))
                
                
                if validation_losses[-1]  == min(validation_losses):
                        print("Saving model with new minimum validation loss")
                        torch.save(model.state_dict(),output_file)  
        
                        
                        print("Saved model successfully!")
                   
        
                print(log)
                run_loss = 0
                accuracy = 0
            
                model.train() 
                
                
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument("-o", dest="output_file", help="File for output of model parameters", required=True, type=str)
    
    
    parser.add_argument("-d", dest="dataset", required=False, type=str, default="pepnn",
                        help="Which dataset to train on, pepnn, pepbind, interpep or bitenet")
    
    parser.add_argument("-sm", dest="saved_model", help="File containing initial params", required=False, type=str, default=None)
    
     
    args = parser.parse_args()
    
    if args.dataset != "pepnn" and args.dataset != "pepbind" and args.dataset != "interpep" and args.dataset != "bitenet":
        raise ValueError("-d must be set to pepnn, pepbind or interpep")
        
    
    edge_features = 39
    node_features = 32
        
    model = FullModel(edge_features, node_features, 6, 64, 6, 
                      64, 128, 64)
    
    if args.saved_model != None:
        
        if torch.cuda.is_available():
            model.load_state_dict(torch.load(os.path.join(os.path.dirname(__file__), args.saved_model)))
        else:
            model.load_state_dict(torch.load(os.path.join(os.path.dirname(__file__), args.saved_model),
                                         map_location='cpu'))
    
    
    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        model.cuda()
        
        
    if args.dataset == "interpep" or args.dataset == "pepbind":
        train(model, 15, args.output_file, args.dataset)
    elif args.dataset == "pepnn":
        train_pepnn(model, 15, args.output_file)
    elif args.dataset == "bitenet":
        train_bitenet(model, 3, args.output_file)
    