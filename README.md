# PepNN

PepNN is a deep learning model for the identification of peptide binding sites given an input protein and a peptide sequence that makes use of a novel attention-based module. There are two variants of the model: PepNN-Struct, which takes as input a protein structure, and PepNN-Seq, which takes as input a protein sequence. The model can also make peptide-agnostic predictions, allowing for the identification of novel peptide binding modules.

## Installation

Installation should take less than 10 minutes. Its recommended that install `pepnn` in a clean python virtual environment (python >= 3.8). PepNN was tested on Ubuntu (18.04) and Windows.

1. Install git-lfs `conda install -c conda-forge git-lfs && git lfs install` 
2. Clone the repository `git clone https://gitlab.com/oabdin/pepnn.git`
3. Change directories to the pepnn repository `cd pepnn`
4. Run `git lfs pull`
5. Install requirements `pip install -r requirements.txt`
6. Install package `pip install .`

## Peptide binding site prediction using PepNN

### PepNN-Struct

To run the test for PepNN-Struct, move to the example file directory (`cd example`) and run the following command. The command should run in less than 10 seconds.

`python ../pepnn_struct/scripts/predict_binding_site.py -prot 4oih.pdb -pep 4oih_peptide.fasta -c A`

The script takes as input the location of a PDB file with a protein structure (`-prot`), an (optional) fasta file with a peptide sequence (`-pep`) and the the chain id of the protein in the PDB file (`-c`). An output folder will be generated with a csv containing the binding probabilities at each residue, a txt file with the overall score for the domain and a PDB file with binding probabilities in the B-factor column.

### PepNN-Seq

To run the test for PepNN-Seq, move to the example file directory (`cd example`) and run the following command. The command should run in less than 30 seconds.

`python ../pepnn_seq/scripts/predict_binding_site.py -prot 4oih_A.fasta -pep 4oih_peptide.fasta `

The script takes as input the location of a fasta file with a protein sequence (`-prot`), an (optional) fasta file with a peptide sequence (`-pep`). An output folder will be generated with a csv containing the binding probabilities at each residue and a txt file with the overall score for the domain.

## Re-training PepNN

Re-training requires PyTorch compilation with CUDA

### Download the training data

1. Change directories to the datasets folder `cd datasets`
2. Download and decompress the relevant data from http://pepnn.ccbr.proteinsolver.org/. The total compressed files take up ~16 GB:
    1. `fragment_data.tar.gz` - the processed protein fragment-protein complex dataset for PepNN pre-training
    2. `pepnn_data.tar.gz` - the processed peptide-protein complex dataset for PepNN
    3. `pepnn_test_data.tar.gz` - the processed peptide-protein complex test dataset for PepNN
    4. `pepbind_data.tar.gz` - the processed PepBind dataset
    5. `interpep_data.tar.gz` - the processed Interpep dataset
    6. `bitenet_data.tar.gz` - the processed BiteNetpp dataset

### Pre-training PepNN-Struct

To pre-train PepNN-Struct on the protein fragment-protein complex data, run the following command. Change directories to pepnn_struct/scripts `cd pepnn_struct/scripts`

`python pre_train.py -o pre_trained_params.pth`

`-o` specifies the file in which the resulting parameters will be stored

`-d` specifies the test dataset on which PepNN will be evaluated, to train on non-redundant data points, can be set to ts092, tsInterpep, tspepbind, or ts125

### Training PepNN-Struct

To train PepNN-Struct run the following command. Change directories to pepnn_struct/scripts `cd pepnn_struct/scripts`

 `python train.py -o pepnn_struct_params.pth -d pepnn -sm ../params/frag_params.pth`

`-o` specifies the file in which the resulting parameters will be stored

`-d` specifies the dataset to train on, can be set to either pepnn, pepbind, bitenet, or interpep

`-sm` the file parameters with which to initalize the model

### Training PepNN-Seq

To train PepNN-Seq run the following command. Change directories to pepnn_seq/scripts `cd pepnn_seq/scripts`

`python train.py -o pepnn_seq_params.pth -d pepnn -sm ../params/frag_params.pth`

`-o` specifies the file in which the resulting parameters will be stored

`-d` specifies the dataset to train on, can be set to either pepnn, pepbind, bitenet or interpep

`-sm` the file parameters with which to initalize the model
    
## Reference

Abdin, O., Nim, S., Wen, H. et al. PepNN: a deep attention model for the identification of peptide binding sites. _Commun Biol_ 5, 503 (2022). https://doi.org/10.1038/s42003-022-03445-2
